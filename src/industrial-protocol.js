'use strict';

const main = {};//Se inicializa el objeto que representa la clase
const fs = require('fs');

/**
 * read
 * Método de lectura del protocolo industrial
 * @param {*} start
 * @param {*} lenght
 * @returns buffer
 * referencia: loadBufer
 */
main.read = (start, length) => {
  return new Promise((resolve,reject)=>{
    //Se activa un retraso de 2 segundos 
    //para simular un proceso pesado de lectura 
    setTimeout(()=>{
        try { 
            const content = fs.readFileSync('./src/buffer.txt');//TXT que simula el buffer generado por el proceso de lectura
            const buf = Buffer.from(content);
            const end =  (start + length < buf.length) ? start + length : buf.length;
            resolve(buf.slice(start,end))
        } 
        catch (e) {
            console.log(e);
            reject(e);
        }
    }, 2000);

  });
};

/**
 * loadBufer
 * Método de carga con la totalidad del contenido del buffer
 * @param {*} start
 * @param {*} lenght
 * @returns buffer
 * referencia: postRequest
 */
main.loadBufer = ()=>{
    return new Promise((resolve,reject)=>{
        main.read(0,4090).then(async(buferData)=>{
            if(!buferData) reject("Error on load");
            resolve(buferData);
        }).catch((e)=>{
            console.log(e)
            reject(e)
        })
    });
};

/**
 * cb0
 * CallBack general
 * @param {*} payload
 * @param {*} start
 * @param {*} lenght
 * @returns String
 * referencia: setCallback
 */
main.cb0 = (payload,start,length)=>{
    return `Response from call to Default function. Start: ${start} - Length: ${length} - Payload: ${payload} `;
};

/**
 * cb1
 * CallBack 1
 * @param {*} payload
 * @param {*} start
 * @param {*} lenght
 * @returns String
 * referencia: setCallback
 */

main.cb1 = (payload,start,length)=>{
    return `Response from call to CB1 function. Start: ${start} - Length: ${length} - Payload: ${payload} `;
};

/**
 * cb2
 * CallBack 2
 * @param {*} payload
 * @param {*} start
 * @param {*} lenght
 * @returns String
 * referencia: setCallback
 */
main.cb2 = (payload,start,length)=>{
    return `Response from call to CB2 function. Start: ${start} - Length: ${length} - Payload: ${payload} `;
};

/**
 * cb3
 * CallBack 3
 * @param {*} payload
 * @param {*} start
 * @param {*} lenght
 * @returns String
 * referencia: setCallback
 */
main.cb3 = (payload,start,length)=>{
    return `Response from call to CB3 function. Start: ${start} - Length: ${length} - Payload: ${payload} `;
};

/**
 * cb4
 * CallBack 4
 * @param {*} payload
 * @param {*} start
 * @param {*} lenght
 * @returns String
 * referencia: setCallback
 */
main.cb4 = (payload,start,length)=>{
    return `Response from call to CB4 function. Start: ${start} - Length: ${length} - Payload: ${payload} `;
};

/**
 * cb5
 * CallBack 5
 * @param {*} payload
 * @param {*} start
 * @param {*} lenght
 * @returns String
 * referencia: setCallback
 */
main.cb5 = (payload,start,length)=>{
    return `Response from call to CB5 function. Start: ${start} - Length: ${length} - Payload: ${payload} `;
};

/**
 * cb6
 * CallBack 6
 * @param {*} payload
 * @param {*} start
 * @param {*} lenght
 * @returns String
 * referencia: setCallback
 */
main.cb6 = (payload,start,length)=>{
    return `Response from call to CB6 function. Start: ${start} - Length: ${length} - Payload: ${payload} `;
};

/**
 * cb7
 * CallBack 7
 * @param {*} payload
 * @param {*} start
 * @param {*} lenght
 * @returns String
 * referencia: setCallback
 */
main.cb7 = (payload,start,length)=>{
    return `Response from call to CB7 function. Start: ${start} - Length: ${length} - Payload: ${payload} `;
};


module.exports = main;
