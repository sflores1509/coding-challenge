'use strict';
const industrialProtocol = require('../src/industrial-protocol');
const main = {};//Se inicializa el objeto que representa la clase
/**
 * setCallback
 * Retorna el payload con su respectivo callback
 * @param {*} req
 * @param {*} res
 * @returns string
 */
 const setCallback = (payload,callback,start,length)=>{

    let response = null;
  
    switch (callback) {
      case 'cb1':
        response = industrialProtocol.cb1(payload,start,length)
        break;
      case 'cb2':
        response = industrialProtocol.cb2(payload,start,length)
        break;
      case 'cb3':
        response = industrialProtocol.cb3(payload,start,length)
        break;
      case 'cb4':
        response = industrialProtocol.cb4(payload,start,length)
        break;
      case 'cb5':
        response = industrialProtocol.cb5(payload,start,length)
        break;
      case 'cb6':
        response = industrialProtocol.cb6(payload,start,length)
        break;
      case 'cb7':
        response = industrialProtocol.cb7(payload,start,length)
        break;
      default:
        response = industrialProtocol.cb0(payload,start,length)
    }
    
    return response;
  };
  
  
  /**
   * parseResponse
   * Recibe la lista de peticiones y llama a las funciones de carga de buffer y de seteo de payloads
   * @param {*} req
   * @param {*} res
   * @returns array
   */
  const parseResponse = (inputList)=>{
    return new Promise((resolve,reject)=>{
      
      industrialProtocol.loadBufer().then(async(BufferData)=>{//Carga la función que retorna el buffer
        const list = inputList.map((info)=> {//Recorre el listado de peticiones
          
          const start =  parseInt(info[0]);
          const length =  parseInt(info[1]);
          const end =  (start + length < BufferData.length) ? start + length : BufferData.length;//Se verifica el length y se parsea 
          const payload = BufferData.slice(start,end);
          const callback = info[2];
       
          return [setCallback(payload,callback,start,length)];
  
        });
  
        Promise.all(list).then((response)=>{
          resolve(response);
        }).catch((e)=>{
          reject(e);
        });
  
      }).catch(e=>{
        reject(e);
      });
  
    });
  };

   /**
   * endpointQueueWorker
   * Recibe la lista de peticiones y llama a la funcion que la parsea
   * @param {*} job
   * @param {*} done
   * @returns array
   */
main.endpointQueueWorker = (job, done) => {
    const inputList = job.data;
    parseResponse(inputList).then((rsp)=>{
        done(null,rsp);
    }).catch((error)=>{
        console.log(error);
        done(error);
    });
  };

module.exports = main;