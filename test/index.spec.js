const app = require('../app');
const request = require('supertest');

describe("POST /tasks", () => {
    describe("given a title and description", () => {
        const inputList = [
            [0,15,"cb1"],
            [12,15,"cb2"],
            [20,32,"cb1"],
            [500,10,"cb4"],
            [4000,10,"cb8"],
            [7,10,"cb7"],
            [80,20,"cb1"],
            [70,2,"cb2"],
            [500,23,"cb3"],
            [6,65,"cb0"],
            [96,74,"cb6"],
            [4,60,"cb0"]
        ]

        // should respond with a 200 code
        test("should respond with a 200 status code", async () => {
            const response = await request(app).post("/").send({inputList});
            expect(response.statusCode).toBe(200);
        });
        // should respond with a an array
        test("should respond an array", async () => {
            const response = await request(app).get("/").send({inputList});
            expect(response.body).toBeInstanceOf(Object);
          });
  
    });
});