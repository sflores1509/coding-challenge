//Archivo donde se instancian las colas
const Queue = require('bull');
const redis ={
    port: process.env.REDIS_PORT,
    host: process.env.REDIS_HOST,
    db: process.env.REDIS_DB
};
const { endpointQueueWorker } = require('./src/workers');
const endpointQueue = new Queue('industrial-protocol',redis);

endpointQueue.process((job, done) => endpointQueueWorker(job, done));

module.exports = { endpointQueue };
