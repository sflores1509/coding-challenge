'use strict';

const functions = {};
const { endpointQueue } = require('../queues');

/**
 * postRequest
 * Recibe la petición vía post y la agrega a la cola
 * @param {*} req
 * @param {*} res
 */
functions.postRequest = async (req, res) => {
  const inputList = req.body.inputList;
  
  try{
    
    await endpointQueue.add(inputList);
    endpointQueue.on('global:completed', (jobId, result) => {
      endpointQueue.close()
      res.send({outputList:result})
    });
  
  }catch(e){

    console.log(e)
    res.status(400).send(e)

  }

};

module.exports = functions;