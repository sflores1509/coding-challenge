'use strict';

const functions = require('./_functions');
const express = require('express');
const router = express.Router();
 
// Industrial protocol
router.post('/', functions.postRequest);

module.exports = router;