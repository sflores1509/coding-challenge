# Coding Challenge Samuel Flores

Desarrollado en NodeJs con Express, la solución implementa una estructura de datos basada en colas para apilar las peticiones y no colapsar el servidor. Para esto se utilizó la librería BullJs. 
Para minimizar las llamadas a la función Read(), se realiza la llamada una sola vez por cada bloque de peticiones y se lee el contenido completo del buffer, que a nivel personal considero es lo más óptimo. Luego se generan los payload realizando la extracción del segmento solicitado, partiendo del índice que llega en la petición y tomando la cantidad de caracteres que se indican como segundo parámetro. Para finalizar se ejecuta el llamado a cada callback con su respectivo payload y se arma el objeto de retorno para devolverlo a la llamada.

## Requerimientos
- NodeJs
- Servidor Redis (para el bull) https://redis.io/docs/getting-started/

## Configuración general
- `npm i` Para instalar las dependencias
- `npm start` Para iniciar el servidor
- `npm test` Para correr las pruebas 

## Archivos y Ficheros
- `/.env` Archivo de variables de entorno
- `/app` Configuración de express
- `/queues` Se instancia la queue (bulljs)
- `/test/` Las pruebas con redis y el cliente Rest
- `/src/` Contiene la cl
- `/src/industrial-protocol` Contiene la clase con los protocolos industriales (read()) y los respectivos callback
- `/src/workers` Contiene los workers con los que trabaja la queue 
- `/http/routes` Contiene la declaración de las rutas de acceso
- `/http/_functions` Contiene el método que recibe la petición
- `/bin/www` Configuración del servidor

## Herramientas adicionales

Extensiones VS Code:

REST Client for Visual Studio Code:
Para ejecutar el endpoint utilizando: `./test/REST client/test.rest`
https://marketplace.visualstudio.com/items?itemName=humao.rest-client
