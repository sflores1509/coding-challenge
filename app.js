'use strict';
//instancia de express
const express = require('express');

const industrialProtocolRouter = require('./http/_routes');

const app = express();
app.use(express.json());

app.use('/', industrialProtocolRouter);

app.all('*', function (req, res) {
    return res.status(404).send("Not Found");
});

module.exports = app;
